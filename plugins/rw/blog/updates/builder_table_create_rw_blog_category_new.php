<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRwBlogCategoryNew extends Migration
{
    public function up()
    {
        Schema::create('rw_blog_category_new', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('new_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('color', 20)->nullable();
            $table->primary(['new_id','category_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rw_blog_category_new');
    }
}
