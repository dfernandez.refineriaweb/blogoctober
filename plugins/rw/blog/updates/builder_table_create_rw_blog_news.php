<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRwBlogNews extends Migration
{
    public function up()
    {
        Schema::create('rw_blog_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 500);
            $table->string('title_seo', 500);
            $table->text('description');
            $table->boolean('status');
            $table->integer('sort_order')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rw_blog_news');
    }
}
