<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRwBlogCategoryNew extends Migration
{
    public function up()
    {
        Schema::table('rw_blog_category_new', function($table)
        {
            $table->dropPrimary(['new_id','category_id']);
            $table->renameColumn('new_id', 'post_id');
            $table->primary(['post_id','category_id']);
        });
    }
    
    public function down()
    {
        Schema::table('rw_blog_category_new', function($table)
        {
            $table->dropPrimary(['post_id','category_id']);
            $table->renameColumn('post_id', 'new_id');
            $table->primary(['new_id','category_id']);
        });
    }
}
