<?php namespace Rw\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRwBlogCategories extends Migration
{
    public function up()
    {
        Schema::create('rw_blog_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('rw_blog_categories');
    }
}
