<?php return [
    'plugin' => [
        'name' => 'blog',
        'description' => ''
    ],
    'fields' => [
        'name' => 'Name',
        'title' => 'Title',
        'title_seo' => 'Title Seo',
        'status' => 'Status',
        'description' => 'Description',
        'category' => 'Category',
        'image' => 'Image'
    ],
    'menu' => [
        'blog' => 'Blog',
        'category' => 'Category',
        'news' => 'News',
        'post' => 'Post',
        'filter' => 'Filter',
        'reorder' => 'Reorder'
    ],
];
