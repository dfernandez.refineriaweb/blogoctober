<?php namespace Rw\Blog;

use System\Classes\PluginBase;
use Event;
use Rw\Blog\Models\Post;
use Rainlab\Blog\Models\Category as RainlabCategoryModel;
use Rainlab\Blog\Controllers\Categories as RainlabCategoriesControllers;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Rw\Blog\Components\Post' => 'posts',
            'Rw\Blog\Components\showpost' => 'ShowPost',
            'RW\Blog\Components\Category' => 'Categories'
        ];
    }

    public function registerSettings()
    {
        //
    }
    public function boot()
    {
        Event::listen('translate.localePicker.translateParams', function ($page, $params, $oldLocale, $newLocale) {
            return Post::translateParams($params, $oldLocale, $newLocale);
        });

        RainlabCategoryModel::extend(function ($model) {
            $model->attachOne['image'] = ['System\Models\File'];
        });

        RainlabCategoriesControllers::extendFormFields(function ($form, $model, $context) {
            $form->addTabFields([
                'image' => [
                    'label' => 'image',
                    'tab' => 'Image',
                    'type' => 'fileupload',
                    'mode' => 'image',
                    'useCaption' => 'true',
                    'span' => 'auto'
                ],
            ]);
        });

        RainlabCategoriesControllers::extendListColumns(function ($list, $model) {
            $list->addColumns([
                'image' => [
                    'label' => 'image',
                    'sortable' => 'false',
                    'type' => 'partial',
                    'path' => 'plugins/rw/blog/controllers/posts/partials/column_image'
                ],
            ]);
        });
    }
}
