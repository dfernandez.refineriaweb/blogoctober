<?php namespace Rw\Blog\Components;

use Cms\Classes\ComponentBase;
use rw\blog\models\Post as ModelPost;

class Post extends ComponentBase
{
    public $posts;
    public function componentDetails()
    {
        return [
            'name'        => 'Post list',
            'description' => 'Show Posts'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->posts = $this->page['posts'] = $this->loadPosts()->where('status', 1);
        // $this->posts = $this->page['posts'] = "hola";
        // dd($this->post);
    }

    protected function loadPosts()
    {
        return ModelPost::all();
    }
}
