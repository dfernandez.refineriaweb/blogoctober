<?php namespace Rw\Blog\Components;

use Cms\Classes\ComponentBase;
use Rw\Blog\Models\Post;

class ShowPost extends ComponentBase
{
    public $post;
    public function componentDetails()
    {
        return [
            'name'        => 'ShowPost',
            'description' => 'Show individual Post'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        try {
            $title_seo = $this->param('title_seo');
            $this->post = Post::transWhere('title_seo', $title_seo)->firstOrFail();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
    }
}
