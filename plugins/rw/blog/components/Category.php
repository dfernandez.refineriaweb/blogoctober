<?php namespace Rw\Blog\Components;

use Cms\Classes\ComponentBase;
use Rw\Blog\Models\Category as ModelCategory;
use Rw\Blog\Models\Post as ModelPost;

class Category extends ComponentBase
{
    public $category;
    public $result;

    public function componentDetails()
    {
        return [
            'name'        => 'Category List',
            'description' => 'Show Categories'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->category = $this->page['categories'] = ModelCategory::all();
    }

    public function onTest()
    {
        $this->result = $this->page['mode'] = post('mode');
        $user = ModelCategory::find($this->result)->posts()->where('status', 1)->get();

        $this->result = $this->page['result'] = $user;
    }
}
