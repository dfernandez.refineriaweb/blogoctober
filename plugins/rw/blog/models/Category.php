<?php namespace Rw\Blog\Models;

use Model;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['name'];

    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'rw_blog_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public $belongsToMany = [
        'posts' => [
            'rw\blog\models\Post',
            'table' => 'rw_blog_category_new',
            'key' => 'category_id',
            'pivot' => ['color']
        ]
    ];
}
