<?php namespace Rw\Blog\Models;

use Model;

/**
 * Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title',
        'description',
            [
                'title_seo',
                'index' => true
            ]
    ];

    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'rw_blog_news';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne = [
        'image' => 'System\Models\File',
    ];
    public $belongsToMany = [
        'categories' => [
            'rw\blog\models\Category',
            'table' => 'rw_blog_category_new',
            'key' => 'post_id',
            'otherKey' => 'category_id',
            'pivot' => ['color']
        ]
    ];

    public static function translateParams($params, $oldLocale, $newLocale)
    {
        $newParams = $params;
        foreach ($params as $paramName => $paramValue) {
            $records = self::transWhere($paramName, $paramValue, $oldLocale)->first();
            if ($records) {
                $records->translateContext($newLocale);
                $newParams[$paramName] = $records->$paramName;
            }
        }
        return $newParams;
    }

    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }
}
