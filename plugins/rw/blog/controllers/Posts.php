<?php namespace Rw\Blog\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Rw\Blog\Models\Post;

class Posts extends Controller
{
    // public $implement = [        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
        'Backend.Behaviors.ReorderController'
        ];

    public $listConfig = [
        'temp' => 'config_list.yaml',
        'layout' => 'config_layouts.yaml'
    ];
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Rw.Blog', 'rw-blog.main', 'side-menu-post');
    }

    public function publish()
    {
        BackendMenu::setContext('Rw.Blog', 'rw-blog-main', 'side-menu-filter');
        $this->asExtension('ListController')->index();
        $this->status=true;
    }

    public function listExtendQuery($query)
    {
        if ($this->status) {
            $query->Status();
        }
    }
}
